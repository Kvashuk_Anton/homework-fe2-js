//slayder
var slideNow = 1;
var slideCount = $('#slidewrapper').children().length;
var slideInterval = 3000;
var navBtnId = 0;
var translateWidth = 0;

$(document).ready(function() {
    var switchInterval = setInterval(nextSlide, slideInterval);

    $('#viewport').hover(function() {
        clearInterval(switchInterval);
    }, function() {
        switchInterval = setInterval(nextSlide, slideInterval);
    });

    $('#next-btn').click(function() {
        nextSlide();
    });

    $('#prev-btn').click(function() {
        prevSlide();
    });

    $('.slide-nav-btn').click(function() {
        navBtnId = $(this).index();

        if (navBtnId + 1 != slideNow) {
            translateWidth = -$('#viewport').width() * (navBtnId);
            $('#slidewrapper').css({
                'transform': 'translate(' + translateWidth + 'px, 0)',
                '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
                '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
            });
            slideNow = navBtnId + 1;
        }
    });
});


function nextSlide() {
    if (slideNow == slideCount || slideNow <= 0 || slideNow > slideCount) {
        $('#slidewrapper').css('transform', 'translate(0, 0)');
        slideNow = 1;
    } else {
        translateWidth = -$('#viewport').width() * (slideNow);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow++;
    }
}

function prevSlide() {
    if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
        translateWidth = -$('#viewport').width() * (slideCount - 1);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow = slideCount;
    } else {
        translateWidth = -$('#viewport').width() * (slideNow - 2);
        $('#slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow--;
    }
}


//drag and drop
let pizzaIngrid = {
    tomat: 0,
    ketchup: 0,
    mayones: 0,
    grib: 0,
    vetchina: 0,
    tom: 0,
}
let foodcost ={
    tomat: 15,
    ketchup: 10,
    mayones: 215,
    grib: 10,
    vetchina: 15,
    tom: 10,
    small: 2,
    middle: 4,
    big: 6,
}

let ingrid = document.getElementsByClassName('ingridient');
let main_pizza = document.getElementById('itog_pizza');
let pizzaSize = 'small';
let total = 2;

window.addEventListener('load', (e) => {
    let total_node = document.querySelector('.cost');
    total_node.innerHTML = total.toFixed(2);

    /**
     * count total
     */
    const updatePrice = () => {
        // calculate ingrid total cost
        const totalWithoutSize = Object.keys(pizzaIngrid).reduce(
            (sum, item) => sum + foodcost[item]*pizzaIngrid[item], 0
        );
        // add size cost to ingrig cost
        total = totalWithoutSize + foodcost[pizzaSize];
        // update ui
        total_node.innerHTML = total.toFixed(2);
    }

    let pizza_size_btns = document.querySelectorAll('.size');
    /**
     * update pizza size, recalculate total
     */
    Array.prototype.map.call(pizza_size_btns, btn => btn.addEventListener('click', e => {
        pizzaSize = e.target.value;
        updatePrice();
    }, false));

    e => {
        // дестркутивное присваивание
        const { target: { value } } = e;
        pizzaIngrid[value] = pizzaIngrid[value] + 1;
        updatePrice();
    }
}, false);
        
